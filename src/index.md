---
title: TheEvilSkeleton
description: "Welcome to my personal website! I talk about computers, GNU/Linux ecosystems, free and open source, and other topics related to software."
layout: general
---

{% include pfp.html %}

{% include command.html command="cat /etc/motd" %}

{{ page.description }} You can read my articles and information about myself in the header.

<h2>Donate</h2>
Feel free to support my work by donating to me on one of the following platforms:
- [GitHub](https://github.com/sponsors/TheEvilSkeleton)
- [LiberaPay](https://liberapay.com/TheEvilSkeleton)

<h2>Socials and Contributions</h2>
You can ~~stalk~~ find me on the following platforms:
- Codeberg: [TheEvilSkeleton](https://codeberg.org/TheEvilSkeleton/)
- GitHub: [TheEvilSkeleton](https://github.com/TheEvilSkeleton)
- GitLab: [TheEvilSkeleton](https://gitlab.com/TheEvilSkeleton)
- GNOME GitLab: [TheEvilSkeleton](https://gitlab.gnome.org/TheEvilSkeleton)
- Mastodon: [@TheEvilSkeleton@treehouse.systems](https://social.treehouse.systems/@TheEvilSkeleton){:rel="external me"}
- Twitter: [@JaakunaGaikotsu](https://twitter.com/JaakunaGaikotsu)
- Reddit: [u/TheEvilSkely](https://www.reddit.com/user/TheEvilSkely)

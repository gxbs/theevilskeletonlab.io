---
title: "Upscaler"
description: "Upscale and enhance images"
layout: marketing
embed_image: "/assets/upscaler.webp"

presentation:
- title: "Private & Secure"
  image: "/assets/upscaler-is-safe.webp"
  description: "Upscaler is fully restricted and cannot access your files, devices, or even the internet."
- title: "Local-only"
  image: "/assets/upscaler-running-locally.webp"
  description: "There's absolutely no internet access. Everything is run on your personal machine."
- title: "Restore Images"
  image: "/assets/upscaled.webp"
  description: "With just a few clicks, you can turn blurry images into clearer and high-quality images."

requirements:
- key: "OS"
  value: "Linux-based operating system"
- key: "Graphics"
  value: "[Vulkan](https://vulkan.gpuinfo.org/listdevices.php?platform=linux)-capable GPU"

project_links:
- title: "Upscaler"
  links:
  - text: "Source code"
    url: "https://gitlab.gnome.org/World/Upscaler"
  - text: "Download app"
    url: "https://flathub.org/apps/details/io.gitlab.theevilskeleton.Upscaler"
  - text: "Chat"
    url: "https://matrix.to/#/%23Upscaler%3Agnome.org"
- title: "Real-ESRGAN ncnn Vulkan"
  links:
  - text: "Source code"
    url: "https://github.com/xinntao/Real-ESRGAN-ncnn-vulkan"
---

<section markdown="1">
<div markdown="1">
# ![](/assets/upscaler-icon.svg) {{ page.title }}

{{ page.description }}
</div>

<nav>
<div><a href="https://flathub.org/apps/details/io.gitlab.theevilskeleton.Upscaler"><img height="64" alt="Download on Flathub" src="https://flathub.org/assets/badges/flathub-badge-en.png"/></a></div>

<div class="badges-list">
<span><a href="https://matrix.to/#/%23Upscaler%3Agnome.org"><img src="https://img.shields.io/matrix/upscaler:matrix.org" alt="Matrix chat"></a></span>
<span><a href="https://flathub.org/apps/details/io.gitlab.theevilskeleton.Upscaler"><img src="https://img.shields.io/flathub/downloads/io.gitlab.theevilskeleton.Upscaler?label=Installs" alt="Installs"></a></span>
<span><a href="https://gitlab.gnome.org/World/Upscaler"><img src="https://img.shields.io/gitlab/v/tag/World/Upscaler?gitlab_url=https%3A%2F%2Fgitlab.gnome.org&amp;label=Upscaler" alt="GitLab tag (latest by SemVer)"></a></span>
</div>
</nav>

{% include image.html
url="/assets/upscaler.webp"
%}

</section>

<section markdown="1" class="breakout-large">
## Features

{% include presentation.html rows=page.presentation %}
</section>

<section markdown="1">
## System Requirements

{% include kvl.html rows=page.requirements %}
</section>

<section markdown="1">
## Technical Details

Upscaler is a graphical interface for, and powered by [Real-ESRGAN ncnn Vulkan], a program that aims to develop practical algorithms for general image/video restoration.

Upscaler is written in Python, using [PyGObject], [GTK4], and [libadwaita]. It is targeted for the [GNOME desktop].
</section>

<section markdown="1">
## Links

{% include bl.html columns=page.project_links %}
</section>

<section markdown="1">
## Licenses

### Upscaler

Copyright © 2022 Upscaler Contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.

### Real-ESRGAN ncnn Vulkan

Copyright © 2021 Xintao Wang

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
</section>

[GNOME GitLab]: https://gitlab.gnome.org/World/Upscaler
[GitHub]: https://github.com/xinntao/Real-ESRGAN-ncnn-vulkan
[Upscaler Contributors]: https://gitlab.gnome.org/World/Upscaler/-/graphs/main
[Real-ESRGAN ncnn Vulkan]: https://github.com/xinntao/Real-ESRGAN-ncnn-vulkan
[GTK4]: https://docs.gtk.org/gtk4
[libadwaita]: https://gnome.pages.gitlab.gnome.org/libadwaita
[PyGObject]: https://pygobject.readthedocs.io/en/latest
[free and open-source software]: https://en.wikipedia.org/wiki/Free_and_open-source_software
[GNOME desktop]: https://www.gnome.org

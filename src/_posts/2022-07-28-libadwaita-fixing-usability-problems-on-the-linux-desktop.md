---
layout: post
title: "libadwaita: Fixing Usability Problems on the Linux Desktop"
description: "
libadwaita is a huge controversy in the Linux desktop community, because of GNOME's stance towards themes.


I have heard a lot of misinformation surrounding GTK4 and libadwaita, mainly based on misunderstanding. I'd like to take some time to explain what GTK4 and libadwaita are, why GNOME decided to go this route and why it's a huge step in the right direction.
"
tags: ["Linux", "Flatpak", "GNOME", "Fedora"]
toc: true
disclaimer: "
1. The premise of this article is to make it easy for people who know little about GTK to understand the situation and motivation behind libadwaita.

1. To provide real world examples, I have to name certain entities and provide links to further explain the topic.

1. I am not speaking on the behalf of GNOME. I am just an outsider who observes the GNOME Project and tries to understand their reasoning and motivation behind controversial changes.

1. This article was written before I was a GNOME Foundation member.
"
redirect_from: /2022/07/28/libadwaita-fixing-usability-problems-on-the-linux-desktop.html
---

## Introduction

{{ page.description }}

## What are GTK4 and libadwaita?
To start off, what exactly is GTK? GTK is a toolkit by the GNOME Project, meaning it provides building blocks for graphical user interfaces (GUI). GTK4 is the 4th iteration of GTK that brings better performance and more features over GTK3.

libadwaita, in layman's term, is a plugin for GTK4 that adds more building blocks tailored for the GNOME desktop environment. These blocks are used to provide more complex widgets to help maintenance, and facilitate development.

## Understanding the Problems With Custom Themes and Complexity of Applications
*Not to confuse with the premise of this paragraph: GNOME has nothing against custom themes themselves. This is merely a side effect with custom themes I want to point out so I can expand onto the actual problems.*

One of the biggest problems regarding custom themes and GNOME applications are that custom themes were unable to catch up with applications. These applications are complex, in the sense that many of the features and UIs have some layers atop to improve certain aspects of the interface, i.e. accessibility, cosmetic, etc., using CSS, as there are plenty of reasons to use an extra layer for the UI, when the base is not enough.

With custom layers in mind, many GNOME developers test with a small range of themes in their limited time, mainly Adwaita and Adwaita-dark --- after all, these projects are developed during free time. This means, many custom themes that were untested in these cases may pose to visual issues; they may look unpleasing because of some content being out of place, have unusual color patterns, may cause accessibility bugs, or even worse, render the application completely nonfunctional.

Furthermore, many GNOME developers want to change the UI a bit here and there to improve its visuals. These minor changes may break custom themes after an update, as themes are generally very sensitive to any kind of changes. They must adapt to fix new issues.

## Distributions Shipping Custom Themes by Default
Many distributions, such as Ubuntu and Pop!\_OS, often either ship themes that have several visual bugs by default or make them very accessible, in the sense that the user can very easily change the theme.

Here are some [examples](https://web.archive.org/web/20220722052234/https://nitter.pussthecat.org/nahuelwexd/status/1433095710598180870):

{% include image.html
url="https://theevilskeleton.fedorapeople.org/Articles/libadwaita-fixing-practical-problems/Black_Fonts_on_Nautilus.jpg"
caption="[Nautilus](https://apps.gnome.org/app/org.gnome.Nautilus) in Dark style on Pop!\_OS. Black font thumbnails are displayed on a dark gray background, resulting in low contrast."
%}

{% include image.html
url="https://theevilskeleton.fedorapeople.org/Articles/libadwaita-fixing-practical-problems/Outplaced_characters_on_Characters.png"
caption="[Characters](https://apps.gnome.org/app/org.gnome.Characters) in Dark style on Pop!\_OS. Black characters are out of place and are displayed on a dark gray background, resulting in low contrast."
%}

{% include image.html
url="https://theevilskeleton.fedorapeople.org/Articles/libadwaita-fixing-practical-problems/White_background_on_Contrast.png"
caption="[Contrast](https://apps.gnome.org/app/org.gnome.design.Contrast) restyled by the app to have an orange background with dark blue text on Pop!\_OS. Entries are not styled due to the custom theme, resulting in their background color to be white."
%}

{% include image.html
url="https://theevilskeleton.fedorapeople.org/Articles/libadwaita-fixing-practical-problems/Ubuntu_Dark_Mode.png"
caption="[Fonts](https://apps.gnome.org/app/org.gnome.font-viewer) on Dark style on Ubuntu. Black font thumbnails are displayed on a dark gray background, resulting in low contrast."
%}

With those examples, we observe that those themes break several aspects of applications, some even making them nonfunctional. When distributions ship custom themes, users get the impression that the application is the issue, but not the theme itself. This gives a bad impression about the application and worsens its reputation, despite the application itself working as intended. Further, many users then contact GNOME developers to fix a problem that doesn't exist within the application. GNOME developers have to refer users to theme developers, or create new styles that patch the theme, in which they have to maintain afterwards. Either way, it increasingly becomes irritating for the maintainer.

Humans, including developers, have limits with repetition. Supporting users about nonexistent issues within the application is typically okay, because it happens. However, when this repetition continues for 5 other times, 10 times, or more, it causes stress and burdens the developer. At the end of the day, many GNOME developers are here to develop applications and fix bugs within their scope of support, not to constantly triage invalid issues, emails and the likes and refer users to other developers. And again, this is done in free time. Many developers have jobs, would like to stay with family, friends or just use their free time to focus on something else with the project.

*Just to clarify: this is about distributions shipping with custom themes by default, not tinkerers knowingly playing with third-party themes.*

## The Request
With these problems in mind, this lead to GNOME contributors to write an [open letter] in 2019, to politely ask distributions to stop shipping custom themes by default, and let users manually apply themes if they ever choose to do so. However, within a couple of years after the letter, nothing had changed: distributions continued to ship custom themes by default, which caused them to break many applications, GNOME developers continued to triage invalid issues and get overburdened, their development would be hindered in some way or another, etc.

## The Solution
As a response, GNOME introduced [libadwaita]. As mentioned at the beginning, libadwaita is a "plugin" for GTK4. It allows developers to use complex widgets with little effort in contrast to using GTK4 purely. However, this convenience comes at a price: "end user freedom", which I will get to later in the article.

GNOME plans to facilitate branding by implementing a recoloring API, to let vendors inject their branding and make it look appealing, as explained in this [article](https://aplazas.pages.gitlab.gnome.org/blog/blog/2020/04/02/coloring-api.html) by Adrien Plazas. Essentially, this means that GNOME developers are starting to put more work to implement proper APIs to let users and distributions customize applications, without the need of hacks.

With the addition of libadwaita, I personally see huge benefits with it and a foreseeable future. I'm currently one of the developers of [Bottles]. Thanks to libadwaita, it helped us decrease the project's codebase, making it easier for us to maintain. It also made Bottles look a lot prettier, and gave us the opportunity to work on other aspects of the project.

## Other Solutions Proposed as Criticism
Now that we know the decision taken by GNOME and why they did so, let's take a look at some arguments people have introduced that GNOME should've taken to prevent the existence of libadwaita and "locking" down theming.

### Warn the User
Many users argue that GNOME developers could have just warned the user in some way; for example, an issue template that asks the user to contact theme developers while they're opening an issue. Unfortunately, it's not that simple. GNOME developers would have to write the template that refers users to theme developers. For that, they would have to assume the user's knowledge, e.g. whether the user knows who to contact, how to contact, what to contact about, etc. And more importantly, they would be the ones to maintain the template.

Additionally and most importantly, issue templates and similar approaches worsen the user experience. GNOME developers would be forced to give unwanted and unexpected warnings and instructions to users. Presumably, the point of an application and distribution is to solve real world problems, not to push away users by making them appalling either socially or technically. By referring users to numerous places, it repulses them, and potentially even discourages them from opening an issue in the first place.

This is also ignoring the fact that whether the user even reads the template, ignores, misreads, etc., and ends up opening an issue in the wrong bug tracker. Even if GNOME developers are not at fault, these events affect them and/or their project, either directly or indirectly. They have very little room to defend or protect themselves.

### No Warranty; No Support
Many users argue that GNOME developers have the right to not provide any support, as the majority of licenses they use have a disclaimer that the project is distributed under no warranty. While this is true, it is not free of consequences. Not providing any support is detrimental to the project. If a developer refuses to support the user, then there's a chance that this event worsens the project's or even the developer's reputation and image.

Of course, not providing support is bound to happen, but minimizing the possibility to provide no support is beneficial to the project, the developer's mental health, and the relationship between developers and users. GNOME developers can't simply provide no support, because there are consequences in doing so.

### Change the License
Another argument I often hear from users is to simply change the license, specifically to a proprietary one. Well, this 100% goes against free and open source philosophy. One of the main reasons why GNOME developers follow the free software philosophy is for ethical reasons.

Many people say that GNOME doesn't care about their users. In my opinion, saying that GNOME developers don't care about users is a false accusation.

GNOME developers typically use copyleft licenses, specifically GNU licenses like GPL. GPL prevents entities from creating proprietary and closed source forks. For example, if I fork (grab the code) a GNOME project that is licensed as GPL, I legally cannot modify and redistribute the software without disclosing the source code, because GPL prohibits forkers from doing so, thereby literally *protecting* user freedom.

While "locking" down theming may worsen user freedom, it prevents distributions from breaking applications, as a result protecting user *experience*. Every decision taken has to have some compromises. GNOME developers are slightly compromising user freedom to protect user experience, as it prevents users from getting a subpar experience out of the box with GNOME applications. In my opinion, this is absolutely worth the compromise.

Frankly, GTK4+libadwaita still allows overwriting CSS, just like any previous GTK version, but requires knowledge with CSS and is very sensitive for the reasons mentioned earlier. This means that GNOME literally does not restrict theming whatsoever, hence me quoting "locking" and "end user freedom" throughout the article.

## Custom Themes in Qt Also Have Issues
I hear many users complaining that these problems only appear with GTK themes and applications, but never with Qt. However, this is untrue; Qt has its own fair share of problems caused by themes.

### qt5ct and Plasma
*Note: [qt5ct] allows you to customize Qt in desktop environments that do not style or integrate Qt, such as Cinnamon and GNOME. It was never meant to be used in KDE Plasma, but the instructions for using qt5ct on those desktop environments cause qt5ct to be used in KDE Plasma anyway.*

qt5ct is a [QPlatformTheme], allowing it to control visual aspects of Qt, as well Qt's behavior when performing actions, such as clicking files. Likewise, [plasma-integration] (KDEPlatformTheme) is KDE's QPlatformTheme, allowing Plasma to apply its appearance and behavior to Qt.

However, Qt can only utilize one QPlatformTheme at a time, meaning using qt5ct in Plasma disables KDEPlatformTheme, causing visual issues and incorrect application behavior.

With qt5ct, we experience visual issues, an example can be seen in System Settings:

{% include image.html
url="https://theevilskeleton.fedorapeople.org/Articles/libadwaita-fixing-practical-problems/kde_syse_qt5ct.png"
caption='[System Settings](https://userbase.kde.org/System_Settings) with qt5ct. Missing indicators in sliders; missing borders around buttons; icons appearing before "Change Wallpaper", "More Appearance Settings..." and "More Behaviour Settings..."; "Most Used Pages" section contains oversized buttons that overlap each other. All of which may make the app appear broken.'
%}

As we can observe with qt5ct, System Settings is now using Qt's default style, resulting in these buttons, as well as enormous radio buttons and checkboxes, and blacked out icons scattered randomly throughout the interface.

Outside of System Settings, applications also no longer recolor their icons according to your color scheme, causing contrast issues in Plasma when using dark color schemes:

{% include image.html
url="https://theevilskeleton.fedorapeople.org/Articles/libadwaita-fixing-practical-problems/qt5ct_kwrite_colour_palette_issue.png"
caption="[KWrite](https://apps.kde.org/kwrite) with Breeze Dark color scheme, with qt5ct. Dark icons are displayed on top of the dark background in the toolbar, resulting in low contrast."
%}

### Kvantum
Kvantum is another popular way to theme Qt. Unlike qt5ct, which is a QPlatformTheme, Kvantum is a [QStyle] (Application Style), meaning Qt applications can integrate exactly as the desktop environment intended. However, Kvantum has problems as well.

Kvantum uses themes which apply predefined color schemes to applications. This works fine for most applications, as they follow the color scheme that Qt provides to applications. However, there are applications that override the color scheme, such as some KDE applications.

This causes those applications to have serious readability issues, such as if a color scheme other than the one intended to be used is applied on some KDE applications:

{% include image.html
url="https://theevilskeleton.fedorapeople.org/Articles/libadwaita-fixing-practical-problems/kvantum_syse.png"
caption='[System Settings](https://userbase.kde.org/System_Settings) with Breeze Dark color scheme on Kvantum with KvArc. Several buttons, such as "Help", "Defaults" and "Reset", and search bars are colored white. They are placed alongside the dark colors restyled by Kvantum, resulting in the app to appear broken.'
%}

As seen here, applications that override colors can potentially worsen the visuals with Kvantum, as Kvantum isn't designed for applications that enforce a different color scheme than is intended by Kvantum.

### Not all Qt Applications can be Themed

To add more, some Qt applications, like Telegram, hard code their own theme, and do not give any easy option to use system themes instead. Should we blame them? Of course not. These applications are really complex in the context of visuals. Allowing system themes means that they will certainly be bound to provide *some* level of support, which can potentially hinder development speed.

## The Fedora Project's Relationship
The Fedora Project managed to properly communicate with GNOME developers and come with an agreement, to make it very appealing to many parties. Fedora Linux uses stock GNOME, with the only addition of adding the Fedora Linux logo at the bottom right of the background. This approach, in my opinion, is what distributions should be going for.

## Conclusion
In conclusion, theming is very sensitive to change, even at the slightest. While users knowingly theming applications were not a problem for GNOME developers, the problem lied in distributions shipping custom themes despite explicitly being asked not to.

It is important to understand that while this article is about GNOME, GTK and libadwaita, this is more of a universal issue than a GTK-specific issue. One of which GNOME is taking a step to solve this fundamental issue with the help of libadwaita, to protect not only developers, but user experience as well, while making it very appealing to several parties.

Before libadwaita, I had no interest in developing GUI applications, as I didn't want to deal with the burden. Thanks to libadwaita, I was convinced that GNOME is building a stable ecosystem that appeals potential developers, contributors and users. Personally, with my limited experience with libadwaita, it was positive. I genuinely applaud all developers and contributors for putting an outstanding amount of work on GTK and libadwaita to build an ecosystem, with a bright future ahead.

This is also a very important lesson to every one: having the freedom to do something doesn't mean it should be done. While it is neat idea to be able to do whatever you want, there is a risk that you can affect people around you, or worse, yourself.

---
*Just because you can, doesn't mean you should.*

---
Edit 1: Rewrite Qt section (Credit to [Dominic Hayes](https://twitter.com/FerenOSDev/)<sup>([Response](https://twitter.com/FerenOSDev/status/1555643960940535808))</sup>)

[Nautilus]: https://apps.gnome.org/app/org.gnome.Nautilus/
[Characters]: https://apps.gnome.org/app/org.gnome.Characters/
[Fonts]: https://apps.gnome.org/app/org.gnome.font-viewer/
[Contrast]: https://apps.gnome.org/app/org.gnome.design.Contrast/
[open letter]: https://stopthemingmy.app/
[libadwaita]: https://gitlab.gnome.org/GNOME/libadwaita
[Bottles]: https://usebottles.com/
[qt5ct]: https://sourceforge.net/projects/qt5ct/
[Kvantum]: https://github.com/tsujan/Kvantum
[QPlatformTheme]: https://doc.qt.io/qt-5/qpa.html
[QtQuickControls]: https://doc.qt.io/qt-5/qtquickcontrols-index.html
[QStyle]: https://doc.qt.io/qt-5/qstyle.html
[plasma-integration]: https://invent.kde.org/plasma/plasma-integration
